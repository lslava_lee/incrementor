//
//  IncrementorTests.swift
//  IncrementorTests
//
//  Created by Vyacheslav on 1/5/18.
//  Copyright © 2018 Vyacheslav. All rights reserved.
//

import XCTest
@testable import Incrementor


class IncrementorTests: XCTestCase {
    
    func testDefaultNumberZero() {
        let defaultNumber = 0
        let incrementor: Incrementor = IncrementorImp(number: defaultNumber)
        
        XCTAssertEqual(incrementor.getNumber(), defaultNumber, "Default number is not zero")
    }
    
    func testDefaultMaxValue() {
        let defaultMaxValue = Int.max
        let incrementor: Incrementor = IncrementorImp(maxValue: defaultMaxValue)
        
        XCTAssertEqual(incrementor.getMaxValue(), defaultMaxValue, "Default maxValue value is not correct")
    }
    
    func testSetMaxValueToTen() {
        let newMaxValue = 10
        let incrementor: Incrementor = IncrementorImp()
        
        incrementor.setMax(value: newMaxValue)
        
        XCTAssertEqual(incrementor.getMaxValue(), newMaxValue, "MaxValue value is not ten")
    }
    
    func testSetMaxValueToZero() {
        let incrementor: Incrementor = IncrementorImp()
        
        incrementor.setMax(value: 0)
        
        XCTAssertNotEqual(incrementor.getMaxValue(), 0, "MaxValue can't set to zero")
    }
    
    func testSetMaxValueLessThanNumber() {
        let incrementor: Incrementor = IncrementorImp(number: 10)
        
        incrementor.setMax(value: 8)
        
        XCTAssertEqual(incrementor.getNumber(), 0, "Number has not reset to zero after maxValue set less than number")
    }
    
    func testIncrementNumber() {
        let incrementor: Incrementor = IncrementorImp()
        
        incrementor.incrementNumber()
        
        XCTAssertEqual(incrementor.getNumber(), 1, "Number has not incremented correctly")
    }
    
    func testNumberReachsMaxValue() {
        let incrementor: Incrementor = IncrementorImp(maxValue: 1)
        
        incrementor.incrementNumber()
        
        XCTAssertEqual(incrementor.getNumber(), 0, "Number has not reset to zero after reaching maxValue")
    }
    
    func testNumberReachsMaxValueAndIncrement() {
        let incrementor: Incrementor = IncrementorImp(maxValue: 2)
        
        incrementor.incrementNumber()
        incrementor.incrementNumber()
        
        incrementor.incrementNumber()
        
        XCTAssertEqual(incrementor.getNumber(), 1, "Number has not increment after reaching maxValue")
    }
    
    func testResetNumberToZero() {
        let incrementor: Incrementor = IncrementorImp(number: 10)
        
        incrementor.resetNumber()
        
        XCTAssertEqual(incrementor.getNumber(), 0, "Number has not reset to zero")
    }
    
}
