//
//  IncrementorFTests.swift
//  IncrementorTests
//
//  Created by Vyacheslav on 1/6/18.
//  Copyright © 2018 Vyacheslav. All rights reserved.
//

import XCTest
@testable import Incrementor

class IncrementorFTests: XCTestCase {
    
    func testDefaultNumberZero() {
        let defaultNumber = 0
        let incrementor: IncrementorF = IncrementorFImp(number: defaultNumber)
        
        XCTAssertEqual(incrementor.getNumber(), defaultNumber, "Default number is not zero")
    }
    
    func testDefaultMaxValue() {
        let initalMaxValue = Int.max
        let incrementor: IncrementorF = IncrementorFImp(maxValue: initalMaxValue)
        
        XCTAssertEqual(incrementor.getMaxValue(), initalMaxValue, "Default maxValue value is not correct")
    }
    
    func testSetMaxValueToTen() {
        let newMaxValue = 10
        let incrementor: IncrementorF = IncrementorFImp()
        
        let maxValue = incrementor.setMax(value: newMaxValue).getMaxValue()
        
        XCTAssertEqual(maxValue, newMaxValue, "MaxValue value is not ten")
    }
    
    func testSetMaxValueToZero() {
        let incrementor: IncrementorF = IncrementorFImp()
        
        let maxValue = incrementor.setMax(value: 0).getMaxValue()
        
        XCTAssertNotEqual(maxValue, 0, "MaxValue can't set to zero")
    }
    
    func testSetMaxValueLessThanNumber() {
        let incrementor: IncrementorF = IncrementorFImp(number: 10)
        
        let number = incrementor.setMax(value: 8).getNumber()
        
        XCTAssertEqual(number, 0, "Number has not reset to zero after maxValue set less than number")
    }
    
    func testIncrementNumber() {
        let incrementor: IncrementorF = IncrementorFImp()
        
        let number = incrementor.incrementNumber().getNumber()
        
        XCTAssertEqual(number, 1, "Number has not incremented correctly")
    }
    
    func testNumberReachsMaxValue() {
        let incrementor: IncrementorF = IncrementorFImp(maxValue: 1)
        
        let number = incrementor.incrementNumber().getNumber()
        
        XCTAssertEqual(number, 0, "Number has not reset to zero after reaching maxValue")
    }
    
    func testNumberReachsMaxValueAndIncrement() {
        let incrementor: IncrementorF = IncrementorFImp(maxValue: 2)
        
        let number = incrementor.incrementNumber().incrementNumber()
            .incrementNumber()
            .getNumber()
        
        XCTAssertEqual(number, 1, "Number has not increment after reaching maxValue")
    }
    
    func testResetNumberToZero() {
        let incrementor: IncrementorF = IncrementorFImp(number: 10)
        
        let number = incrementor.resetNumber().getNumber()
        
        XCTAssertEqual(number, 0, "Number has not reset to zero")
    }
    
    func testResetNumberToZeroAndCheckMaxValue() {
        let defaultMaxValue = 20
        let incrementor: IncrementorF = IncrementorFImp(number: 10, maxValue: defaultMaxValue)
        
        let maxValue = incrementor.resetNumber().getMaxValue()
        
        XCTAssertEqual(maxValue, defaultMaxValue, "MaxValue is different than defaultMaxValue")
    }

}
