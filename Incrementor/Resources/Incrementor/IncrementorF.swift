//
//  IncrementorF.swift
//  Incrementor
//
//  Created by Vyacheslav on 1/6/18.
//  Copyright © 2018 Vyacheslav. All rights reserved.
//

import Foundation


// Mixed FP Style

/// An `IncrementorF` is used to increment number.
public protocol IncrementorF {
    
    /// Get incrementation number.
    ///
    /// - returns: incrementation number.
    ///
    func getNumber() -> Int
    
    /// Get max value of incrementation number.
    ///
    /// - returns: max value.
    ///
    func getMaxValue() -> Int
    
    /**
     Set max value of incrementation number.
     The value could'n be zero.
     If value less than number, the number will be reset
     
     - parameter value: The new maximum value.
     
     - returns: An instance of `IncrementorF`.
     
     */
    func setMax(value: Int) -> IncrementorF
    
    /// Increment number.
    /// If number reachs maxValue, the number will be reset
    ///
    /// - returns: An instance of `IncrementorF`.
    ///
    func incrementNumber() -> IncrementorF
    
    /// Reset number.
    /// Reset number to zero
    ///
    /// - returns: An instance of `IncrementorF`.
    ///
    func resetNumber() -> IncrementorF
    
}


/// an `IncrementorFImp` is implementation of `IncrementorF`.
public struct IncrementorFImp: IncrementorF {
    
    /// Number which increments
    private let number: Int
    
    /// MaxValue of increment number
    private let maxValue: Int
    
    /**
     Initialize a `IncrementorFImp`.
     
     - parameter number: Incrementation number. Default number is 0.
     - parameter maxValue: Max value of incrementation number. Default value is max of Int.
     
     - returns: An initialized `IncrementorFImp`.
     
     */
    public init(number: Int = 0, maxValue: Int = Int.max) {
        self.number = number
        self.maxValue = maxValue
    }
    
    public func getNumber() -> Int {
        return number
    }
    
    public func getMaxValue() -> Int {
        return maxValue
    }
    
    public func setMax(value: Int) -> IncrementorF {
        guard value > 0 else {
            return self
        }
        let newItem = IncrementorFImp(number: number, maxValue: value)
        guard value >= number else {
            return newItem.resetNumber()
        }
        return newItem
    }
    
    public func incrementNumber() -> IncrementorF {
        let incrementedNumber = number + 1
        if incrementedNumber == maxValue {
            return resetNumber()
        }
        return IncrementorFImp(number: incrementedNumber, maxValue: maxValue)
    }
    
    public func resetNumber() -> IncrementorF {
        return IncrementorFImp(maxValue: maxValue)
    }
    
}
