//
//  Incrementor.swift
//  Incrementor
//
//  Created by Vyacheslav on 1/5/18.
//  Copyright © 2018 Vyacheslav. All rights reserved.
//

import Foundation


// OOP Style

/// An `Incrementor` is used to increment number.
public protocol Incrementor {
    
    /// Get incrementation number.
    ///
    /// - returns: incrementation number.
    ///
    func getNumber() -> Int
    
    /// Get max value of incrementation number.
    ///
    /// - returns: max value.
    ///
    func getMaxValue() -> Int
    
    /**
     Set max value of incrementation number.
     The value could'n be zero.
     If value less than number, the number will be reset
     
     - parameter value: The new maximum value.
     
     */
    func setMax(value: Int)
    
    /// Increment number.
    /// If number reachs maxValue, the number will be reset
    func incrementNumber()
    
    /// Reset number.
    /// Reset number to zero
    func resetNumber()
    
}


/// an `IncrementorImp` is implementation of `Incrementor`.
public class IncrementorImp: Incrementor {
    
    /// Number which increments
    private var number: Int
    
    /// MaxValue of increment number
    private var maxValue: Int
    
    /**
     Initialize a `IncrementorImp`.
     
     - parameter number: Incrementation number. Default number is 0.
     - parameter maxValue: Max value of incrementation number. Default value is max of Int.
     
     - returns: An initialized `IncrementorImp`.
     
    */
    public init(number: Int = 0, maxValue: Int = Int.max) {
        self.number = number
        self.maxValue = maxValue
    }
    
    public func getNumber() -> Int {
        return number
    }
    
    public func getMaxValue() -> Int {
        return maxValue
    }
    
    public func setMax(value: Int) {
        guard value > 0 else {
            return
        }
        maxValue = value
        if number > maxValue {
            resetNumber()
        }
    }
    
    public func incrementNumber() {
        number += 1
        if number == maxValue {
            resetNumber()
        }
    }
    
    public func resetNumber() {
        number = 0
    }

}
