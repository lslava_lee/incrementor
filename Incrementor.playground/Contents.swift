//: Playground - noun: a place where people can play

import UIKit
import IncrementorLib

// OOP style Incrementor

let incrementor: Incrementor = IncrementorImp()
incrementor.setMax(value: 10)
incrementor.incrementNumber()
incrementor.getNumber()
incrementor.getMaxValue()

// Mixed FP style Incrementor

let incrementorF: IncrementorF = IncrementorFImp()
incrementorF.setMax(value: 10).incrementNumber().getNumber()
incrementorF.setMax(value: 1).incrementNumber().getNumber()
incrementorF.incrementNumber().incrementNumber().resetNumber()
